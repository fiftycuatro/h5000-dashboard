var ApplicationView = React.createClass({
  propTypes: {
     content: React.PropTypes.element.isRequired,
  },

  navBarClass: "navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse",

  render: function() {

     return (
        React.createElement('nav', {className: this.navBarClass}
     );
  }

});
