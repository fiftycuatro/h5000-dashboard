var LineChart = React.createClass({
  data: [
    {age: 30, index: 0},
    {age: 38, index: 1},
    {age: 34, index: 2},
    {age: 12, index: 3}
  ],

  chartSeries: [
     {
       field: 'age',
       name:  'Age',
       color: '#ff7f0e',
       style: {
         "stroke-width":    2,
         "stroke-opacity": .2,
         "fill-opacity":   .2
       }
     }
  ],

  x: function(d) {
    return d.index;
  },

  render: function() {
     return (
       React.createElement(ReactD3Basic.LineChart, {
         width: 400,
         height: 400,
         data: this.data,
         chartSeries: this.chartSeries,
         x: this.x
       })
     );
  }
});
